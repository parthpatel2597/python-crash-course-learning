import math

guest_list = [ 'Aryabhatta', 'Jon Lennon', 'Albert Einstein', 'Slyvester Stallone', 'Ed Winters', 'Stephen Hawking' ]

print("We just found a bigger table!")

guest_list.insert(0, 'Buckminister Fuller')
middle = (math.floor(len(guest_list)/2))
#print(middle)
guest_list.insert(middle, 'Socrates')
guest_list.append('Galileo')
guest_list.append('Richard Feynman')

#var = len(guest_list)
#print(var)
#Reference: printing variable in various ways with or inside a string
#print("Number of people coming to dinner = ", var, ' lolpodo')
print(f"Number of people coming to dinner = {len(guest_list)}")
