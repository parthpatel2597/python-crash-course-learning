import math
import numpy as np

#                                  1         2       3        4         5         6          7          8        9          10         11             12
tortured_animals_by_humans = [ 'chickens', 'cows', 'pigs', 'fish', 'lobsters', 'crabs', 'octopuses',
        'lions', 'tigers', 'jaguars', 'gorrilas', 'exotic birds' ]
one_third_size = math.floor(len(tortured_animals_by_humans)/3)

## One Third size manual calculation of chunks
start=1
end=5
length = list(range(start,end+1))
for i in length:
    print(f"{i}   ", end='')
print("\n")

def print_parth_chunks(full_list):
## I liked below approach but still i prefer having the split for a 7 element array to be 3,2,2
## instead of 3,3,1. This solution is referenced here - https://stackoverflow.com/a/36526443/5306761
#    chunk_size = math.floor(len(full_list)/3)
#    if (chunk_size*3) < len(full_list):
#        chunk_size += 1

    chunk_size = math.floor(len(full_list)/3)
    middle_chunk_starting_index = chunk_size
    middle_chunk_ending_index = chunk_size * 2
    
## Better proper way of chunking (the exact logic output at end as used by numpy.array_split)
## defined here - https://stackoverflow.com/a/36526485/5306761 which means for a 
## 5 element array splits will be 2,2,1 and for 7 element array splits will be 3,2,2
# 3 is hard-coded here as I know I am always splitting my list here in parts of three
    remainder_bonus = len(full_list) - (chunk_size * 3)
    if remainder_bonus == 1:
        middle_chunk_starting_index += 1
        middle_chunk_ending_index += 1
    if remainder_bonus == 2:
        middle_chunk_starting_index += 1
        middle_chunk_ending_index += 2

    print(f"Size of list = {len(length)} one_third = {chunk_size}")

    first_chunk = full_list[:middle_chunk_starting_index]
    second_chunk = full_list[middle_chunk_starting_index:middle_chunk_ending_index]
    third_chunk = full_list[middle_chunk_ending_index:]

    print(f"[array({first_chunk}), array({second_chunk}), array({third_chunk})]")


#print(f"Size of list = {len(length)}
#        one_third = {one_third_size}
#        starting index = {starting_index} ending_index = {ending_index}")
#print(f"Start of chunk = {length[starting_index]} end of chunk = {length[ending_index]}")
print_parth_chunks(length)
print(f"{np.array_split(length, 3)}")
#print(f"Size of list = {len(tortured_animals_by_humans)} One-third of list is = {one_third}")

#print(f"First three animals tortured by humans are: {tortured_animals_by_humans[:3]}")
#print(f"Middle three animals tortured by humans are: {tortured_animals_by_humans[:]}")


print("\nImagine there wouldn't be any exploitation of wildlife and nature by humans... \
        \nYou may say that I'm a dreamer, but I am not alone...\n  - John Lennon")
