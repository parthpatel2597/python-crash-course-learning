banned_users = ['hemal', 'sutto', 'britto']

def is_user_banned(user):
    if user in banned_users:
        print(f"{user.title()} is banned.")
    else:
        print(f"{user.title()} is not banned.")

is_user_banned('hemal')
is_user_banned('manoj')
