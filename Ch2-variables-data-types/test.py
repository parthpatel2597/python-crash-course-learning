# References - Python format string and calling methods in format strings and composing custom messages
name = "hula gula"
print(name.title())
print(name.upper())
print(name.lower())

print("")

first_name = "parth"
last_name = "patel"

full_name = f"{first_name.title()} {last_name.title()}"
print(full_name)

print(f"Hello there! My name is {first_name.title()} {last_name.title()}. Dattebayo Python now :D")
print(f"Hello there! My name is {full_name}. Dattebayo Python now :D")

var = ' test string '
print("\nWithout rstrip:" + var + "==")
print("With rstrip:" + var.rstrip() + "==")

print("\nWithout lstrip:" + var + "==")
print("\nWith lstrip:" + var.lstrip() + "==")

print("\nWithout strip:" + var + "==")
print("\nWith strip:" + var.strip() + "==")
