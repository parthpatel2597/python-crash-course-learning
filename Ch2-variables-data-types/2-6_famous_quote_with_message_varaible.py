famous_person = 'master oogway'
quote = "You are too concerned with what was, and what will be.\n\t Yesterday is history, tomorrow is a mystery, but today is a gift. That is why it is called the Present!"
print(f'{famous_person.title()} said, "{quote}"')
