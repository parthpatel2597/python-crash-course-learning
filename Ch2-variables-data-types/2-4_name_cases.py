name = 'patel Patel'

print(f"Unformatted name = {name}")

print(f"Lower case name = {name.lower()}")
print(f"Upper case name = {name.upper()}")
print(f"Title case name = {name.title()}")
